package com.dsys.appointme.exception;

public class UserNotFoundException extends Exception{

	private static final long serialVersionUID = -2082246019904042754L;

	public UserNotFoundException(String exceptionMessage){
		super(exceptionMessage);
	}
	
}
