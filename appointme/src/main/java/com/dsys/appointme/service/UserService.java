package com.dsys.appointme.service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.dsys.appointme.model.User;
import com.dsys.appointme.repository.UserRepository;

@Service("userDetailsService")
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository; 
	
	Logger logger = LogManager.getLogger(UserService.class);
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		CustomUserDetails userDetails=null;
		User user=userRepository.findOneByUsername(username);
		
		if(user !=null){
			userDetails = new CustomUserDetails();
			userDetails.setUser(user);
			logger.info(user.getPassword());

		}
		
		else{
			throw new UsernameNotFoundException("invalid username and password");
		}
		
	/*	List<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(0, new SimpleGrantedAuthority("ROLE_ADMIN"));
		grantedAuthorities.add(1, new SimpleGrantedAuthority("ROLE_DOCTOR"));
		grantedAuthorities.add(2,new SimpleGrantedAuthority("ROLE_GROUP_ADMIN"));
		grantedAuthorities.add(3, new SimpleGrantedAuthority("ROLE_GROUP_VIEWER"));
		grantedAuthorities.add(3, new SimpleGrantedAuthority("ROLE_SITE_ADMIN"));
		*/
		return new org.springframework.security.core.userdetails.User(
				user.toString(),user.getPassword(),user.getAuthorities());
	}
}