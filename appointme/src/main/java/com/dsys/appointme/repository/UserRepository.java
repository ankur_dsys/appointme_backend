package com.dsys.appointme.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dsys.appointme.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findOneByUsername(String username);
}
